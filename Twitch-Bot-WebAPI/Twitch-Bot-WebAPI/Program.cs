﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Twitch_Bot_WebAPI.Classes;
using Twitch_Bot_WebAPI.Handler;
using Twitch_Bot_WebAPI.Modules;

namespace Twitch_Bot_WebAPI
{
    public class Program
    {
        public static List<Streamer> streamerList;
        public static FillListHandler fillListHandler;
        public static Livestream ls;
        public static DiscordHandler disco;
        public static DiscordSocketClient client;

        static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            Timer time = new Timer();
            streamerList = new List<Streamer>();
            fillListHandler = new FillListHandler();
            ls = new Livestream();
            disco = new DiscordHandler();
            
            streamerList = fillListHandler.FillStreamerList(streamerList);

            time.Elapsed += Time_Elapsed;
            time.Interval = 60000;
            
            client = new DiscordSocketClient();
            client.Log += Log;
            await client.LoginAsync(TokenType.Bot, DiscordInfo.BotToken, true);
            await client.StartAsync();
            await Task.Delay(5000);

            streamerList = await ls.CheckStatus(streamerList, disco, client);
            time.Enabled = true;
            time.Start();

            if(Console.ReadKey(true).Key == ConsoleKey.Enter)
            {
                Environment.Exit(0);
            }
        }

        private Task Log(LogMessage msg)
        {
            try
            {
                Console.WriteLine(msg.Message.ToString());
            }catch{ Console.WriteLine("An error occourred"); }
            return Task.CompletedTask;
        }

        private async static void Time_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                List<Streamer> streamers = new List<Streamer>();
                streamers = fillListHandler.FillStreamerList(streamers);
                List<Streamer> temp = new List<Streamer>();

                foreach (var item in streamers)
                {
                    if (streamerList.Exists(x => x.display_name == item.display_name) == false)
                    {
                        streamerList.Add(item);
                    }
                }
                foreach (var item in streamerList)
                {
                    if (streamers.Exists(x => x.display_name == item.display_name) == false)
                    {
                        temp.Add(item);
                    }
                }

                foreach (var item in temp)
                {
                    streamerList.Remove(item);
                }


                temp.Clear();
                streamers.Clear();

                streamerList = await ls.CheckStatus(streamerList, disco, client);
                Console.WriteLine($@"{DateTime.Now.ToString()}: livestream list updated");
            }
            catch { Console.WriteLine("An error occourred"); }
        }
    }
}


