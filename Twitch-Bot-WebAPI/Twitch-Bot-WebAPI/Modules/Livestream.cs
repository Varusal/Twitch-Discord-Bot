﻿using Discord.WebSocket;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitch_Bot_WebAPI.Classes;
using Twitch_Bot_WebAPI.Handler;

namespace Twitch_Bot_WebAPI.Modules
{
    public class Livestream
    {
        RootObject results;
        RestClient client;
        RestRequest request;
        IRestResponse response;

        public Livestream()
        {
            client = new RestClient();
            client.BaseUrl = new Uri(TwitchInfo.BaseUrl);
        }

        public async Task<List<Streamer>> CheckStatus(List<Streamer> streamerList, DiscordHandler disco, DiscordSocketClient dcClient)
        {
            foreach (var item in streamerList)
            {
                ConfigRequest(item.display_name);
                response = client.Execute(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    results = JsonConvert.DeserializeObject<RootObject>(response.Content);
                    if (results.stream != null)
                    {
                        if (item.live != true)
                        {
                            item.live = true;
                            if (results.stream.channel.display_name.Contains('_'))
                            {
                                await disco.SendMessage($@"{results.stream.channel.display_name.Replace('_',' ')}is now live and is playing {results.stream.game}! {results.stream.channel.url}", dcClient);
                                Console.WriteLine($@"{DateTime.Now.ToString()}: {results.stream.channel.display_name.Replace('_', ' ')}is now live and is playing {results.stream.game}! {results.stream.channel.url}");
                            }
                            else
                            {
                                await disco.SendMessage($@"{results.stream.channel.display_name} is now live and is playing {results.stream.game}! {results.stream.channel.url}", dcClient);
                                Console.WriteLine($@"{DateTime.Now.ToString()}: {results.stream.channel.display_name} is now live and is playing {results.stream.game}! {results.stream.channel.url}");
                            }                            
                        }
                    }
                    else
                    {
                        item.live = false;
                    }                    
                }
            }

            return streamerList;
        }

        private void ConfigRequest(string display_name)
        {
            request = new RestRequest();
            request.Method = Method.GET;
            request.Resource = display_name;
            request.AddHeader("client-id", TwitchInfo.ClientId);
            request.RequestFormat = DataFormat.Json;
        }
    }
}
