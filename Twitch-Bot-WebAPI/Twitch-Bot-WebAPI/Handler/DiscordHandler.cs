﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch_Bot_WebAPI.Handler
{
    public class DiscordHandler
    {
        public async Task SendMessage(string message, DiscordSocketClient client)
        {
            if (message.Contains("SirMorokei"))
            {
                await ((ISocketMessageChannel)client.GetChannel(419230470300237844)).SendMessageAsync(message);
            }
            else
            {
                await ((ISocketMessageChannel)client.GetChannel(421999968300040194)).SendMessageAsync(message);
            }
        }

        private Task Log(LogMessage msg)
        {
            try
            {
                Console.WriteLine(msg.ToString());
            }
            catch { Console.WriteLine("An error occourred"); }
            return Task.CompletedTask;
        }
    }
}
