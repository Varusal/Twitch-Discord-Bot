﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Twitch_Bot_WebAPI.Classes;

namespace Twitch_Bot_WebAPI.Handler
{
    public class FillListHandler
    {
        public RegistryKey key;
        public List<Streamer> FillStreamerList(List<Streamer> streamerList)
        {
            List<string> keys = new List<string>();
            RegChecker();

            keys = key.GetValueNames().ToList<string>();

            foreach (var item in keys)
            {
                Streamer str = new Streamer() { display_name = item, live = false };
                streamerList.Add(str);
            }

            return streamerList;
        }

        internal void RegChecker()
        {
            key = Registry.CurrentUser.OpenSubKey(RegistryInfo.SoftwareSubkey, true);
            if (!SubKeyChecker(key, RegistryInfo.OrganizationSubkey))
            {
                key.CreateSubKey(RegistryInfo.OrganizationSubkey);
            }
            key = key.OpenSubKey(RegistryInfo.OrganizationSubkey, true);

            if (!SubKeyChecker(key, RegistryInfo.ApplicationSubkey))
            {
                key.CreateSubKey(RegistryInfo.ApplicationSubkey);
            }
            key = key.OpenSubKey(RegistryInfo.ApplicationSubkey, true);
        }

        internal bool SubKeyChecker(RegistryKey key, string subKey)
        {
            bool available = false;
            foreach (var item in key.GetSubKeyNames())
            {
                if (item == subKey)
                {
                    available = true;
                }
            }

            return available;
        }
    }
}
