﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch_Bot_WebAPI
{
    public class RootObject
    {
        public Stream stream { get; set; }
        public Links3 _links { get; set; }
    }
}
