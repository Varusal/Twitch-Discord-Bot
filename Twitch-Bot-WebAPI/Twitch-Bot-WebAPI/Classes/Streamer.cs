﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch_Bot_WebAPI.Classes
{
    public class Streamer
    {
        public string display_name { get; set; }
        public bool live { get; set; }
    }
}
