﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddStreamer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<string> streamers;
        public RegistryKey key;
        public MainWindow()
        {
            InitializeComponent();
            streamers = new List<string>();
            key = Registry.CurrentUser.OpenSubKey(@"Software\SirMorokei\TAFD", true);
            Refresh();
        }

        internal void GetStreamers()
        {
            streamers = key.GetValueNames().ToList<string>();
        }

        internal void ShowStreamers()
        {
            lbStreamers.ItemsSource = streamers;
        }

        internal void Refresh()
        {
            GetStreamers();
            ShowStreamers();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                key.DeleteValue(lbStreamers.SelectedItem.ToString());
                Refresh();
            }
            catch { Refresh(); }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            key.SetValue(tbxStreamer.Text, "");
            tbxStreamer.Clear();
            Refresh();
        }
    }
}
